<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function dept()
    {
    	return $this->hasOne('App\Department', 'id', 'department');
    }
}
