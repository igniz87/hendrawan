<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    public function dept()
    {
    	return $this->hasOne('App\Department', 'id', 'department_id');
    }
    public function supp()
    {
    	return $this->hasOne('App\Supplier', 'id', 'supplier_id');
    }
    public function proc()
    {
    	return $this->hasOne('App\Procurement', 'id', 'procurement_id');
    }
    public function cat()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }
    public function member()
    {
    	return $this->hasOne('App\Member', 'id', 'user_id');
    }
}
