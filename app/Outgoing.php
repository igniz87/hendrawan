<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outgoing extends Model
{

    public function inv()
    {
        return $this->hasOne('App\Inventory', 'id', 'inventory_id');
    }


}
