<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MutationDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mutations_detail';

    public function inv()
    {
    	return $this->hasOne('App\Inventory', 'id', 'inventory_id');
    }
    public function deptFrom()
    {
        return $this->hasOne('App\Department', 'id', 'department_from');
    }
    public function deptTo()
    {
        return $this->hasOne('App\Department', 'id', 'department_to');
    }
}