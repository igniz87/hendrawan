<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutation extends Model
{
    public function detail()
    {
        return $this->hasOne('App\MutationDetail', 'mutation_id', 'id');
    }
}
