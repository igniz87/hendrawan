<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inventory;
use App\Outgoing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class OutgoingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    if(Input::get('print'))
        {
            if(Input::get('print')=='true')
            {
                $outgoings = Outgoing::orderBy('id', 'desc')->get();
                Excel::create('outgoings', function($excel) use($outgoings) {
                    $excel->sheet('Sheet 1', function($sheet) use($outgoings) {
                        $sheet->fromArray($outgoings);
                    });
                })->export('xls');
            }

        }
        else
        {
            $outgoings = Outgoing::orderBy('id', 'desc')->paginate(10);

            return view('outgoings.index', compact('outgoings'));
        }

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $inventory = Inventory::where('status',1)->pluck('assets_number','id');
        return view('outgoings.create',compact('inventory'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$outgoing = new Outgoing();

		$outgoing->inventory_id = $request->input("inventory_id");
        $outgoing->keterangan = $request->input("keterangan");

        $inventory = Inventory::find($request->input('inventory_id'));
        $inventory->status = 0;
        $inventory->save();

		$outgoing->save();

		return redirect()->route('outgoings.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$outgoing = Outgoing::findOrFail($id);

		return view('outgoings.show', compact('outgoing'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$outgoing = Outgoing::findOrFail($id);

		return view('outgoings.edit', compact('outgoing'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$outgoing = Outgoing::findOrFail($id);

		$outgoing->inventory_id = $request->input("inventory_id");
        $outgoing->keterangan = $request->input("keterangan");

		$outgoing->save();

		return redirect()->route('outgoings.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$outgoing = Outgoing::findOrFail($id);
		$outgoing->delete();

		return redirect()->route('outgoings.index')->with('message', 'Item deleted successfully.');
	}

}
