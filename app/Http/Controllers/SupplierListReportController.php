<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;

class SupplierListReportController extends Controller
{
    public function index()
    {
        // $procurements = Procurement::orderBy('id', 'desc')->paginate(10);

        $suppliers = Supplier::orderBy('id','desc')->get();

        return view('supplierlistreport.index',compact('suppliers'));
    }
}
