<?php

namespace App\Http\Controllers;

use App\Mutation;
use Illuminate\Http\Request;

use App\Http\Requests;

class AssetMutationReportController extends Controller
{
    public function index()
    {
        $mutations = Mutation::orderBy('id','desc')->with('detail')->get();
        //dd($mutations);
        return view('assetmutationreport.index',compact('mutations'));
    }
}
