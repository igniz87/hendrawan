<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Inventory;
use App\Procurement;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $procurements = Procurement::orderBy('id', 'desc')->paginate(10);

		return view('reports.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		

		return view('reports.result');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
	    $print = $request->input('print');

	    /*dd($print);*/
		$start = $request->input('tanggal_mulai');
		$end = $request->input('tanggal_akhir');
		$filter = $request->input('filter');
		$keyword = $request->input('keyword');
		if($start != '' && $end != '')
		{
			$filter_date = true;
		}
		else
		{
			$filter_date = false;
		}


		switch ($filter) {
			case 'all':
				if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->orderBy('id','desc')->paginate(1000);
				if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'category':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('category_id','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('category_id','=',$keyword)->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::where('category_id','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::where('category_id','=',$keyword)->orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'mobility':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('mobility_type','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::orderBy('id', 'desc')->get();
                }
				// dd($request);
			}
			else
			{
				$inventories = $inv = Inventory::where('mobility_type','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'department':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('department_id','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('department_id','=',$keyword)->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::where('department_id','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::where('department_id','=',$keyword)->orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'member':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('user_id','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('user_id','=',$keyword)->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::where('user_id','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::where('user_id','=',$keyword)->orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'supplier':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('supplier_id','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('supplier_id','=',$keyword)->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::where('supplier_id','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::where('supplier_id','=',$keyword)->orderBy('id', 'desc')->get();
                }
			}
				break;
			case 'procurement':
			if($filter_date == true)
			{
				$inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('procurement_id','=',$keyword)->orderBy('id','desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::whereBetween('tanggal_beli',array($start,$end))->where('procurement_id','=',$keyword)->orderBy('id','desc')->get();
                }
			}
			else
			{
				$inventories = $inv = Inventory::where('procurement_id','=',$keyword)->orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::where('procurement_id','=',$keyword)->orderBy('id', 'desc')->get();
                }
			}
				break;
			default:
				$inventories = $inv = Inventory::orderBy('id', 'desc')->paginate(1000);
                if(NULL != $print)
                {
                    $inventories = $inv = Inventory::orderBy('id', 'desc')->get();
                }
				break;
		}

        if(null != $print)
        {
            //dd($inventories);

            $finale = array();
            foreach ($inventories as $inventory) {
                $darray = $inventory->toArray();
                $permonth = floor($inventory->harga_beli/($inventory->life*12));
                $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $inventory->tanggal_beli);
                $endDate = \Carbon\Carbon::now();
                $datediff = $startDate->diffInDays($endDate);
                $monthdiff = floor($datediff/30);
                $currentprice = $inventory->harga_beli - ($monthdiff*$permonth);
                $darray['permonth'] = $permonth;
                $darray['runningmonth'] = $monthdiff;
                $darray['currentprice'] = $currentprice;
                $finale[] = $darray;

            }
            //dd($finale);
            // Generate and return the spreadsheet
            Excel::create('reports', function($excel) use($finale) {
                $excel->sheet('Sheet 1', function($sheet) use($finale) {
                    $sheet->fromArray($finale);
                });
            })->export('xls');
        }
		return view('reports.result',compact('inventories'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		

		return view('reports.table');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$procurement = Procurement::findOrFail($id);

		return view('procurements.edit', compact('procurement'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$procurement = Procurement::findOrFail($id);

		$procurement->name = $request->input("name");

		$procurement->save();

		return redirect()->route('procurements.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$procurement = Procurement::findOrFail($id);
		$procurement->delete();

		return redirect()->route('procurements.index')->with('message', 'Item deleted successfully.');
	}
}
