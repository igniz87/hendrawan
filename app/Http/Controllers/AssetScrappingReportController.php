<?php

namespace App\Http\Controllers;

use App\Outgoing;
use Illuminate\Http\Request;

use App\Http\Requests;

class AssetScrappingReportController extends Controller
{
    public function index()
    {
        $outgoings = Outgoing::orderBy('id','desc')->with('inv')->get();

        return view('assetscrappingreport.index',compact('outgoings'));
    }
}
