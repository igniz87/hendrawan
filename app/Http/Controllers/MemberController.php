<?php namespace App\Http\Controllers;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Member;
use Illuminate\Http\Request;

class MemberController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$members = Member::orderBy('id', 'desc')->paginate(10);

		return view('members.index', compact('members'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$department = Department::pluck('name','id');
		
		return view('members.create',compact('department'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$member = new Member();

		$member->name = $request->input("name");
        $member->department = $request->input("department");
        $member->nik = $request->input("nik");

		$member->save();

		return redirect()->route('members.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$member = Member::findOrFail($id);

		return view('members.show', compact('member'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$member = Member::findOrFail($id);
		$department = Department::pluck('name','id');
		
		return view('members.edit', compact('member','department'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$member = Member::findOrFail($id);

		$member->name = $request->input("name");
        $member->department = $request->input("department");
        $member->nik = $request->input("nik");

		$member->save();

		return redirect()->route('members.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$member = Member::findOrFail($id);
		$member->delete();

		return redirect()->route('members.index')->with('message', 'Item deleted successfully.');
	}

}
