<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;

use App\Http\Requests;

class MemberListReportController extends Controller
{
    public function index()
    {
        $members = Member::orderBy('id','desc')->get();
        return view('memberlistreport.index',compact('members'));
    }
}
