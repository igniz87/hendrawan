<?php namespace App\Http\Controllers;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Inventory;
use App\Member;
use App\Mutation;
use App\MutationDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MutationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$mutations = Mutation::orderBy('id', 'desc')->paginate(10);

		return view('mutations.index', compact('mutations'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$inventory = Inventory::pluck('assets_number','id');
		$department = Department::pluck('name','id')->prepend('Please Select','');
		$member = Member::pluck('name','id')->prepend('Please Select','');
		return view('mutations.create',compact('inventory','department','member'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$mutation = new Mutation();

		$mutation->keterangan = $request->input("keterangan");
        $mutation->tanggal_mutasi = $request->input("tanggal_mutasi");
        $mutation->nomor_pma = $request->input("nomor_pma");
		$mutation->save();

		$id = $mutation->id;

		foreach ($request->input("inventories") as $value) {
			
			DB::table('mutations_detail')->insert(
			    [
			    'mutation_id' => $id,
			    'inventory_id' => $value,
			    'department_from' => $request->input("department_from"),
			    'department_to' => $request->input("department_to"),
			    'user_from' => $request->input("user_from"),
			    'user_to' => $request->input("user_to")
			    ]
			);
			$inventory = Inventory::findOrFail($value);
	        $inventory->department_id = $request->input("department_to");
	        $inventory->user_id = $request->input("user_to");
	        $inventory->save();
	        
		}

		return redirect()->route('mutations.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$mutation = Mutation::findOrFail($id);
		$details = MutationDetail::where('mutation_id', $id)
               ->get();
        $_department_to = $details[0]->department_to;
		$_department_from = $details[0]->department_from;
		$_user_to = $details[0]->user_to;
        $_user_from = $details[0]->user_from;
        
        $department_to = Department::findOrFail($_department_to);
		$department_from = Department::findOrFail($_department_from);
        $user_to = Member::findOrFail($_user_to);
        $user_from = Member::findOrFail($_user_from);

        $array = [
        'dept_to' => $department_to->name,
        'dept_from' => $department_from->name,
        'user_to' => $user_to->name,
        'user_from' => $user_from->name
        ];




		return view('mutations.show', compact('mutation','details','array'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mutation = Mutation::findOrFail($id);

		return view('mutations.edit', compact('mutation'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$mutation = Mutation::findOrFail($id);

		$mutation->keterangan = $request->input("keterangan");
        $mutation->tanggal_mutasi = $request->input("tanggal_mutasi");

		$mutation->save();

		return redirect()->route('mutations.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$mutation = Mutation::findOrFail($id);
		$mutation->delete();

		return redirect()->route('mutations.index')->with('message', 'Item deleted successfully.');
	}

}
