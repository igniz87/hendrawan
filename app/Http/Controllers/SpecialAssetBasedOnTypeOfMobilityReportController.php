<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class SpecialAssetBasedOnTypeOfMobilityReportController extends Controller
{
    public function index()
    {
        $assets = DB::select('select inventories.mobility_type, categories.name, count(inventories.id) as total from inventories inventories left join categories categories on inventories.category_id = categories.id GROUP BY inventories.mobility_type , categories.name');

        //dd($assets);
        return view('specialassetbasedontypeofmobilityreport.index',compact('assets'));
    }
}
