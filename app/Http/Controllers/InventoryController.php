<?php namespace App\Http\Controllers;

use App\Category;
use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Inventory;
use App\Member;
use App\Procurement;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inventories = Inventory::where('status',1)->orderBy('id', 'desc')->paginate(10);

		return view('inventories.index', compact('inventories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$department = Department::pluck('name','id');
		$procurement = Procurement::pluck('name','id');
		$supplier = Supplier::pluck('name','id');
		$category = Category::pluck('name','id');
		$member = Member::pluck('name','id');

		$life = array(2=>2 , 4=>4,8=>8,16=>16);
		return view('inventories.create',compact('department','procurement','supplier','category','member','life'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$inventory = new Inventory();

		$inventory->name = $request->input("name");
        $inventory->description = $request->input("description");
        $inventory->harga_beli = $request->input("harga_beli");
        $inventory->tanggal_beli = $request->input("tanggal_beli");
        $inventory->supplier_id = $request->input("supplier_id");
        $inventory->procurement_id = $request->input("procurement_id");
        $inventory->category_id = $request->input("category_id");
        $inventory->mobility_type = $request->input("mobility_type");
        $inventory->department_id = $request->input("department_id");
        $inventory->user_id = $request->input("user_id");
        $inventory->life = $request->input("life");
        $inventory->keterangan = $request->input("keterangan");
        
        $cat_keyword = DB::table('categories')->where('id',$request->input('category_id'))->get();
        $cat_character = $cat_keyword[0]->name;
        $cat_character = $cat_character[0];

        $maxid = DB::table('inventories')->where('id', DB::raw("(select max(id) from inventories)"))->get();
		if(sizeof( $maxid)==0)
		{
			$maxid =0;
		}
		else
		{
			$maxid = $maxid[0]->id;
		}
		
		$increment = str_pad($maxid + 1, 6, 0, STR_PAD_LEFT);
		$inventory->assets_number = $cat_character.$increment;
		$inventory->save();

		return redirect()->route('inventories.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$inventory = Inventory::findOrFail($id);

		return view('inventories.show', compact('inventory'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$inventory = Inventory::findOrFail($id);

		$department = Department::pluck('name','id');
		$procurement = Procurement::pluck('name','id');
		$supplier = Supplier::pluck('name','id');
		$category = Category::pluck('name','id');
		$member = Member::pluck('name','id');
        $life = array(2=>2 , 4=>4,8=>8,16=>16);

        return view('inventories.edit', compact('inventory','department','procurement','supplier','category','member','life'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$inventory = Inventory::findOrFail($id);

		$inventory->name = $request->input("name");
        $inventory->description = $request->input("description");
        $inventory->harga_beli = $request->input("harga_beli");
        $inventory->tanggal_beli = $request->input("tanggal_beli");
        $inventory->supplier_id = $request->input("supplier_id");
        $inventory->procurement_id = $request->input("procurement_id");
        $inventory->mobility_type = $request->input("mobility_type");
        $inventory->category_id = $request->input("category_id");
        $inventory->department_id = $request->input("department_id");
        $inventory->user_id = $request->input("user_id");
        $inventory->life = $request->input("life");
        $inventory->keterangan = $request->input("keterangan");
        // $inventory->assets_number = $request->input("assets_number");

		$inventory->save();

		return redirect()->route('inventories.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$inventory = Inventory::findOrFail($id);
		$inventory->delete();

		return redirect()->route('inventories.index')->with('message', 'Item deleted successfully.');
	}

}
