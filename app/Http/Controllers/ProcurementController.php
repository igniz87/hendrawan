<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Procurement;
use Illuminate\Http\Request;

class ProcurementController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$procurements = Procurement::orderBy('id', 'desc')->paginate(10);

		return view('procurements.index', compact('procurements'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('procurements.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$procurement = new Procurement();

		$procurement->name = $request->input("name");

		$procurement->save();

		return redirect()->route('procurements.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$procurement = Procurement::findOrFail($id);

		return view('procurements.show', compact('procurement'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$procurement = Procurement::findOrFail($id);

		return view('procurements.edit', compact('procurement'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$procurement = Procurement::findOrFail($id);

		$procurement->name = $request->input("name");

		$procurement->save();

		return redirect()->route('procurements.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$procurement = Procurement::findOrFail($id);
		$procurement->delete();

		return redirect()->route('procurements.index')->with('message', 'Item deleted successfully.');
	}

}
