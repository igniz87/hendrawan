<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Inventory;

class AssetBalanceReportController extends Controller
{
    public function index()
    {
        $inventories = Inventory::orderBy('id','desc')->get();
        return view('assetbalancereport.index',compact('inventories'));
    }
}
