<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;

use App\Http\Requests;

class AllAssetListReportController extends Controller
{
    public function index()
    {
        // $procurements = Procurement::orderBy('id', 'desc')->paginate(10);

        $inventories = Inventory::orderBy('id','desc')->get();

        return view('allassetlistreport.index',compact('inventories'));
    }

}
