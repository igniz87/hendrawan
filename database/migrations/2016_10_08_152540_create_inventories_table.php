<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->double('harga_beli');
            $table->date('tanggal_beli');
            $table->string('supplier_id');
            $table->string('mobility_type');
            $table->string('procurement_id');
            $table->string('category_id');
            $table->string('department_id');
            $table->string('user_id');
            $table->text('keterangan');
            $table->string('assets_number');
            $table->integer('status')->default(1);
            $table->integer('life')->default(2);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventories');
	}

}
