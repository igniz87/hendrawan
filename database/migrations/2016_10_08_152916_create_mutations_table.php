<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mutations', function(Blueprint $table) {
            $table->increments('id');
            $table->text('keterangan');
            $table->date('tanggal_mutasi');
            $table->string('nomor_pma');
            $table->timestamps();
        });

        Schema::create('mutations_detail', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('mutation_id');
            $table->integer('inventory_id');
            $table->integer('department_from');
            $table->integer('department_to');
            $table->integer('user_from');
            $table->integer('user_to');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mutations');
		Schema::drop('mutations_detail');
	}

}
