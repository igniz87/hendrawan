<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class DepartmentTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('departments')->delete();
 
        $faker = Faker::create();
        $departments = array(
            ['name' => 'Accounting' , 'description' => $faker->text(50),'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Business' , 'description' => $faker->text(50),'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'IT' , 'description' => $faker->text(50),'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Human Resources' , 'description' => $faker->text(50),'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'General Affairs' , 'description' => $faker->text(50),'created_at' => new DateTime, 'updated_at' => new DateTime],

        );
        // Uncomment the below to run the seeder
        DB::table('departments')->insert($departments);
    }

}