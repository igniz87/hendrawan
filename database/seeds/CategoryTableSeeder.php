<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('categories')->delete();
 
        $categories = array(
            ['name' => 'Computer' , 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Desk' , 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Planner' , 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Automobiles' , 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Tools' , 'created_at' => new DateTime, 'updated_at' => new DateTime],

        );
        // Uncomment the below to run the seeder
        DB::table('categories')->insert($categories);
    }

}