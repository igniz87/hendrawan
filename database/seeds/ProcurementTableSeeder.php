<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ProcurementTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('procurements')->delete();
 
        $procurements = array(
            ['name' => 'Buy','created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'Rent','created_at' => new DateTime, 'updated_at' => new DateTime],
          	['name' => 'Giveaway','created_at' => new DateTime, 'updated_at' => new DateTime],
        );
        // Uncomment the below to run the seeder
        DB::table('procurements')->insert($procurements);
    }

}