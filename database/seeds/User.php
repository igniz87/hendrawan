<?php


use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
 
        $users = array(
            ['name' => 'Super Admin', 'email' => 'superadmin@hendrawan.dev', 'password' => Hash::make('hendrawan') , 'created_at' => new DateTime, 'updated_at' => new DateTime],
            
        );
        // Uncomment the below to run the seeder
        DB::table('users')->insert($users);

        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('secret'),
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]);
        }

    }
}
