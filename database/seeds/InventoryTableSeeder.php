<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory as TestDummy;

class InventoryTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('inventories')->delete();
 
        
        $faker = Faker::create();
        foreach (range(1,200) as $index) {
        	//Generate a timestamp using mt_rand.
			$timestamp = mt_rand(1, time());
			 
			//Format that timestamp into a readable date string.
			$randomDate = date("d M Y", $timestamp);

		$category_id = rand(1,4);

		$cat_keyword = DB::table('categories')->where('id',$category_id)->get();
        $cat_character = $cat_keyword[0]->name;
        $cat_character = $cat_character[0];

        $maxid = DB::table('inventories')->where('id', DB::raw("(select max(id) from inventories)"))->get();
		if(sizeof( $maxid)==0)
		{
			$maxid =0;
		}
		else
		{
			$maxid = $maxid[0]->id;
		}
		
		$increment = str_pad($maxid + 1, 6, 0, STR_PAD_LEFT);

            DB::table('inventories')->insert([
                'name' => $faker->name,
                'description' =>$faker->sentence,
                'harga_beli' => rand(100000,10000000),
                'tanggal_beli' => $randomDate,
                'supplier_id' => rand(0,18),
                'procurement_id' => rand(1,3),
                'category_id' => $category_id,
                'department_id'=> rand(1,4),
                'keterangan'=> $faker->sentence,
                'user_id' => rand(1,9),
                'life' => 2,
                'assets_number' => $cat_character.$increment,
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]);
        }
    }

}