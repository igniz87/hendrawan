<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SupplierTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('suppliers')->delete();
 
        
        $faker = Faker::create();
        foreach (range(1,20) as $index) {
            DB::table('suppliers')->insert([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber,
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]);
        }
    }

}