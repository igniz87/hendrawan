<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(User::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(ProcurementTableSeeder::class);
        $this->call(MemberTableSeeder::class);
        //$this->call(InventoryTableSeeder::class);
    }
}
