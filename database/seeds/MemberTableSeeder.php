<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class MemberTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('members')->delete();
 
        
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('members')->insert([
                'name' => $faker->name,
                'department' => rand(1,5),
                'nik' => $faker->isbn13,
                'created_at' => new DateTime, 
                'updated_at' => new DateTime
            ]);
        }
    }

}