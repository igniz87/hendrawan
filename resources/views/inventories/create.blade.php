@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Inventories / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('inventories.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('description')) has-error @endif">
                       <label for="description-field">Description</label>
                    <input type="text" id="description-field" name="description" class="form-control" value="{{ old("description") }}"/>
                       @if($errors->has("description"))
                        <span class="help-block">{{ $errors->first("description") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('harga_beli')) has-error @endif">
                       <label for="harga_beli-field">Buy Value</label>
                    <input type="number" id="harga_beli-field" name="harga_beli" class="form-control" value="{{ old("harga_beli") }}"/>
                       @if($errors->has("harga_beli"))
                        <span class="help-block">{{ $errors->first("harga_beli") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tanggal_beli')) has-error @endif">
                       <label for="tanggal_beli-field">Purchase Date</label>
                    <input type="text" id="tanggal_beli-field" name="tanggal_beli" class="form-control date-picker" value="{{ old("tanggal_beli") }}"/>
                       @if($errors->has("tanggal_beli"))
                        <span class="help-block">{{ $errors->first("tanggal_beli") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('supplier_id')) has-error @endif">
                       <label for="supplier_id-field">Supplier_id</label>
                       {!! Form::select('supplier_id', $supplier,old("supplier_id"),['class' => 'form-control','id'=>'supplier_id-field']) !!}
                       @if($errors->has("supplier_id"))
                        <span class="help-block">{{ $errors->first("supplier_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('procurement_id')) has-error @endif">
                       <label for="procurement_id-field">Procurement_id</label>
                       {!! Form::select('procurement_id', $procurement,old("procurement_id"),['class' => 'form-control','id'=>'procurement_id-field']) !!}
                       @if($errors->has("procurement_id"))
                        <span class="help-block">{{ $errors->first("procurement_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('category_id')) has-error @endif">
                       <label for="category_id-field">Category_id</label>
                       {!! Form::select('category_id', $category,old("category_id"),['class' => 'form-control','id'=>'category_id-field']) !!}
                        @if($errors->has("category_id"))
                        <span class="help-block">{{ $errors->first("category_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('mobility_type')) has-error @endif">
                       <label for="mobility_type-field">mobility_type</label>
                       {!! Form::select('mobility_type', ['low'=>'Low','middle'=>'Middle','high'=>'High'],old("mobility_type"),['class' => 'form-control','id'=>'mobility_type-field']) !!}
                        @if($errors->has("mobility_type"))
                        <span class="help-block">{{ $errors->first("mobility_type") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('department_id')) has-error @endif">
                       <label for="department_id-field">Department_id</label>
                       {!! Form::select('department_id', $department,old("department_id"),['class' => 'form-control','id'=>'department_id-field']) !!}
                       @if($errors->has("department_id"))
                        <span class="help-block">{{ $errors->first("department_id") }}</span>
                       @endif
                    </div>

                <div class="form-group @if($errors->has('life')) has-error @endif">
                    <label for="life-field">Life</label>
                    {!! Form::select('life', $life,old("life"),['class' => 'form-control','id'=>'life-field']) !!}
                    @if($errors->has("life"))
                        <span class="help-block">{{ $errors->first("life") }}</span>
                    @endif
                </div>

                    <div class="form-group @if($errors->has('user_id')) has-error @endif">
                       <label for="user_id-field">User_id</label>
                       {!! Form::select('user_id', $member,old("user_id"),['class' => 'form-control','id'=>'user_id-field']) !!}
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('keterangan')) has-error @endif">
                       <label for="keterangan-field">Notes</label>
                    <textarea class="form-control" id="keterangan-field" rows="3" name="keterangan">{{ old("keterangan") }}</textarea>
                       @if($errors->has("keterangan"))
                        <span class="help-block">{{ $errors->first("keterangan") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('inventories.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
                   format: 'yyyy-mm-dd',
                   todayHighlight: true
    });
  </script>
@endsection
