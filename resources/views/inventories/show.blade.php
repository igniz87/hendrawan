@extends('layout')
@section('header')
<div class="page-header">
        <h1>Inventories / Show #{{$inventory->id}}</h1>
        {{--<form action="{{ route('inventories.destroy', $inventory->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('inventories.edit', $inventory->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>--}}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$inventory->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="description">DESCRIPTION</label>
                     <p class="form-control-static">{{$inventory->description}}</p>
                </div>
                    <div class="form-group">
                     <label for="harga_beli">BUY VALUE</label>
                     <p class="form-control-static">{{number_format( $inventory->harga_beli , 0 , '.' , ',' )}}</p>
                </div>
                    <div class="form-group">
                     <label for="tanggal_beli">PURCHASE DATE</label>
                     <p class="form-control-static">{{$inventory->tanggal_beli}}</p>
                </div>
                    <div class="form-group">
                     <label for="supplier_id">SUPPLIER_ID</label>
                     <p class="form-control-static">{{$inventory->supp->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="procurement_id">PROCUREMENT_ID</label>
                     <p class="form-control-static">{{$inventory->proc->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="category_id">CATEGORY_ID</label>
                     <p class="form-control-static">{{$inventory->cat->name}}</p>
                </div>
                <div class="form-group">
                     <label for="category_id">MOBILITY TYPE</label>
                     <p class="form-control-static">{{$inventory->mobility_type}}</p>
                </div>
                    <div class="form-group">
                     <label for="department_id">DEPARTMENT_ID</label>
                     <p class="form-control-static">{{$inventory->dept->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="user_id">USER_ID</label>
                     <p class="form-control-static">{{$inventory->member->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="keterangan">NOTES</label>
                     <p class="form-control-static">{{$inventory->keterangan}}</p>
                </div>
                    <div class="form-group">
                     <label for="assets_number">ASSETS_NUMBER</label>
                     <p class="form-control-static">{{$inventory->assets_number}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('inventories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection