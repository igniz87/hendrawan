@extends('layout')

@section('header')
    <div class="page-header clearfix">


    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($inventories->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>DESCRIPTION</th>
                        <th>BUY VALUE</th>
                        <th>PURCHASE DATE</th>
                        <th>SUPPLIER</th>
                        <th>PROCUREMENT</th>
                        <th>CATEGORY</th>
                        <th>DEPARTMENT</th>
                        <th>USER</th>
                        <th>LIFE</th>
                        <th>PER MONTH</th>
                        <th>CURRENT</th>
                        <th>NOTES</th>
                        <th>ASSETS NUMBER</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($inventories as $inventory)
                            <tr>
                    <td>{{$inventory->id}}</td>
                    <td>{{$inventory->name}}</td>
                    <td>{{$inventory->description}}</td>
                    <td>{{number_format( $inventory->harga_beli , 0 , '.' , ',' )}}</td>
                    <td>{{$inventory->tanggal_beli}}</td>
                    <td>{{$inventory->supp->name}}</td>
                    <td>{{$inventory->proc->name}}</td>
                    <td>{{$inventory->cat->name}}</td>
                    <td>{{$inventory->dept->name}}</td>
                    <td>{{$inventory->member->name}}</td>
                    <td>{{$inventory->life}} years</td>

                    <?php

                    $permonth = floor($inventory->harga_beli/($inventory->life*12));
                    $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $inventory->tanggal_beli);
                    $endDate = \Carbon\Carbon::now();
                    $datediff = $startDate->diffInDays($endDate);
                    $monthdiff = floor($datediff/30);
                    $currentprice = $inventory->harga_beli - ($monthdiff*$permonth);

                    ?>
                    <td>{{number_format( $permonth , 0 , '.' , ',' )}}</td>
                    <td>{{number_format( $currentprice , 0 , '.' , ',' )}}</td>

                    <td>{{$inventory->keterangan}}</td>
                    <td>{{$inventory->assets_number}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $inventories->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>

            @endif
            <a class="btn btn-link pull-right" href="{{ route('reports.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
        </div>
    </div>

@endsection
            