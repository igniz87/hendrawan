@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Reports
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('reports.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group @if($errors->has('filter')) has-error @endif">
                       <label for="filter-field">filter</label>
                    {!! Form::select('filter', ['all'=>'all','category'=>'category','department'=>'department','member'=>'member','mobility'=>'mobility','supplier'=>'supplier','procurement'=>'procurement','procurement'=>'procurement'],old("filter"),['class' => 'form-control','id'=>'filter-field','onchange'=>'fetch_select(this.value)']); !!}
                          @if($errors->has("filter"))
                        <span class="help-block">{{ $errors->first("filter") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('keyword')) has-error @endif">
                       <label for="keyword-field">keyword</label>
                       {!! Form::select('keyword', [],old("keyword"),['class' => 'form-control','id'=>'keyword-field']); !!}
                       @if($errors->has("keyword"))
                        <span class="help-block">{{ $errors->first("keyword") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tanggal_mulai')) has-error @endif">
                       <label for="tanggal_mulai-field">tanggal_mulai</label>
                    <input type="text" id="tanggal_mulai-field" name="tanggal_mulai" class="form-control date-picker" value="{{ old("tanggal_mulai") }}"/>
                       @if($errors->has("tanggal_mulai"))
                        <span class="help-block">{{ $errors->first("tanggal_mulai") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tanggal_akhir')) has-error @endif">
                       <label for="tanggal_akhir-field">tanggal_akhir</label>
                    <input type="text" id="tanggal_akhir-field" name="tanggal_akhir" class="form-control date-picker" value="{{ old("tanggal_akhir") }}"/>
                       @if($errors->has("tanggal_akhir"))
                        <span class="help-block">{{ $errors->first("tanggal_akhir") }}</span>
                       @endif
                    </div>
                <div class="form-group @if($errors->has('print')) has-error @endif">
                    <label for="print-field">print</label>
                    {!! Form::checkbox('print',old("print"),['class' => 'form-control','id'=>'print-field']) !!}
                    @if($errors->has("print"))
                        <span class="help-block">{{ $errors->first("print") }}</span>
                    @endif
                </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
                   format: 'yyyy-mm-dd',
                   todayHighlight: true
    });
  </script>

  <script type="text/javascript">
      function fetch_select(val)
{
  $('#keyword-field').html('');

  if(val ==='category')
   $.ajax({
     type: 'GET',
     url: '/getCategory',
     data: {},
     success: function (response) {
       $.each(response,function(key, value) 
      {
        $('#keyword-field').append($('<option>', { 
        value: value.id,
        text : value.name 
    }));
      });
     }
   });


  if(val ==='department')
   $.ajax({
     type: 'GET',
     url: '/getDepartment',
     data: {},
     success: function (response) {
       $.each(response,function(key, value) 
      {
        $('#keyword-field').append($('<option>', { 
        value: value.id,
        text : value.name 
    }));
      });
     }
   });


  if(val ==='member')
   $.ajax({
     type: 'GET',
     url: '/getMember',
     data: {},
     success: function (response) {
       $.each(response,function(key, value) 
      {
        $('#keyword-field').append($('<option>', { 
        value: value.id,
        text : value.name 
    }));
      });
     }
   });


  if(val ==='supplier')
   $.ajax({
     type: 'GET',
     url: '/getSupplier',
     data: {},
     success: function (response) {
       $.each(response,function(key, value) 
      {
        $('#keyword-field').append($('<option>', { 
        value: value.id,
        text : value.name 
    }));
      });
     }
   });


  if(val ==='procurement')
   $.ajax({
     type: 'GET',
     url: '/getProcurement',
     data: {},
     success: function (response) {
       $.each(response,function(key, value) 
      {
        $('#keyword-field').append($('<option>', { 
        value: value.id,
        text : value.name 
    }));
      });
     }
   });


   if(val ==='mobility')
   $.ajax({
     type: 'GET',
     url: '/getMobility',
     data: {},
     success: function (response) {
       $('#keyword-field').append($('<option>', { 
        value: 'low',
        text : 'low' 
    }));
       $('#keyword-field').append($('<option>', { 
        value: 'medium',
        text : 'medium' 
    }));
       $('#keyword-field').append($('<option>', { 
        value: 'high',
        text : 'high' 
    }));

     }
   });

}


  </script>

<script type="text/javascript">
    jQuery(document).ready(function($) {

  if (window.history && window.history.pushState) {

    $('#filter-field').val('all');

  }
});
</script>
@endsection