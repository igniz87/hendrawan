@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" />
@endsection
@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Reports
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table id="my-table" width="100%">
  <thead>
    <th>Id</th>
    <th>Name</th>
    <th>Description</th>
    <th>Buy Value</th>
    <th>Purchase Date</th>
    <th>Notes</th>
    <th>Assets Number</th>
  </thead>
  <tbody>
  </tbody>
</table>

        </div>
    </div>

@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
  

<script type="text/javascript">
    jQuery(document).ready(function($) {

$.ajax({
  url: '/getAll',
  success: function(data){
    console.log(data);
    $('#my-table').dynatable({
      table: {
    defaultColumnIdStyle: 'underscore'
  },
      dataset: {
        records: data
      },
      features: {
    paginate: false,
    search: false,
    recordCount: true,
    perPageSelect: false
  }
    });
  }
});

});
</script>
@endsection