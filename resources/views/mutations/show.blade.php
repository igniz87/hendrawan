@extends('layout')
@section('header')
<div class="page-header">
        <h1>Mutations / Show #{{$mutation->id}}</h1>
        {{--<form action="{{ route('mutations.destroy', $mutation->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                --}}{{-- <a class="btn btn-warning btn-group" role="group" href="{{ route('mutations.edit', $mutation->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a> --}}{{--
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>--}}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                     <label for="keterangan">KETERANGAN</label>
                     <p class="form-control-static">{{$mutation->keterangan}}</p>
                </div>
                    <div class="form-group">
                     <label for="tanggal_mutasi">TANGGAL_MUTASI</label>
                     <p class="form-control-static">{{$mutation->tanggal_mutasi}}</p>
                </div>
                <div class="form-group">
                    <label for="tanggal_mutasi">NOMOR_PMA</label>
                    <p class="form-control-static">{{$mutation->nomor_pma}}</p>
                </div>
                <div class="form-group">
                     <label for="tanggal_mutasi">Departmen from</label>
                     <p class="form-control-static">{{$array['dept_from']}}</p>
                </div>
                <div class="form-group">
                     <label for="tanggal_mutasi">Department_to</label>
                     <p class="form-control-static">{{$array['dept_to']}}</p>
                </div>
                <div class="form-group">
                     <label for="tanggal_mutasi">User from</label>
                     <p class="form-control-static">{{$array['user_from']}}</p>
                </div>
                <div class="form-group">
                     <label for="tanggal_mutasi">User to</label>
                     <p class="form-control-static">{{$array['user_to']}}</p>
                </div>
                <div class="form-group">
                     <label for="keterangan">List of Inventories</label>
                     @foreach ($details as $detail)
                    <p>{{ $detail->inv->name }}</p>
                    @endforeach
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('mutations.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection