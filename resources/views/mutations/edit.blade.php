@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Mutations / Edit #{{$mutation->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('mutations.update', $mutation->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('keterangan')) has-error @endif">
                       <label for="keterangan-field">Notes</label>
                    <textarea class="form-control" id="keterangan-field" rows="3" name="keterangan">{{ is_null(old("keterangan")) ? $mutation->keterangan : old("keterangan") }}</textarea>
                       @if($errors->has("keterangan"))
                        <span class="help-block">{{ $errors->first("keterangan") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tanggal_mutasi')) has-error @endif">
                       <label for="tanggal_mutasi-field">Mutation Date</label>
                    <input type="text" id="tanggal_mutasi-field" name="tanggal_mutasi" class="form-control date-picker" value="{{ is_null(old("tanggal_mutasi")) ? $mutation->tanggal_mutasi : old("tanggal_mutasi") }}"/>
                       @if($errors->has("tanggal_mutasi"))
                        <span class="help-block">{{ $errors->first("tanggal_mutasi") }}</span>
                       @endif
                    </div>

                <div class="form-group @if($errors->has('nomor_pma')) has-error @endif">
                    <label for="nomor_pma-field">PMA Number</label>
                    <input type="text" id="nomor_pma-field" name="nomor_pma" class="form-control" value="{{ is_null(old("nomor_pma")) ? $mutation->nomor_pma : old("nomor_pma") }}"/>
                    @if($errors->has("nomor_pma"))
                        <span class="help-block">{{ $errors->first("nomor_pma") }}</span>
                    @endif
                </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('mutations.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
      format: 'yyyy-mm-dd',
      todayHighlight: true
    });
  </script>
@endsection
