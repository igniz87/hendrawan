@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Mutations / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('mutations.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('keterangan')) has-error @endif">
                       <label for="keterangan-field">NOTES</label>
                    <textarea class="form-control" id="keterangan-field" rows="3" name="keterangan">{{ old("keterangan") }}</textarea>
                       @if($errors->has("keterangan"))
                        <span class="help-block">{{ $errors->first("keterangan") }}</span>
                       @endif
                    </div>

                <div class="form-group @if($errors->has('nomor_pma')) has-error @endif">
                    <label for="nomor_pma-field">PMA Number</label>
                    <input type="text" id="nomor_pma-field" name="nomor_pma" class="form-control" value="{{ old("nomor_pma") }}"/>
                    @if($errors->has("nomor_pma"))
                        <span class="help-block">{{ $errors->first("nomor_pma") }}</span>
                    @endif
                </div>

                    <div class="form-group @if($errors->has('tanggal_mutasi')) has-error @endif">
                       <label for="tanggal_mutasi-field">Mutation Date</label>
                    <input type="text" id="tanggal_mutasi-field" name="tanggal_mutasi" class="form-control date-picker" value="{{ old("tanggal_mutasi") }}"/>
                       @if($errors->has("tanggal_mutasi"))
                        <span class="help-block">{{ $errors->first("tanggal_mutasi") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('department_from')) has-error @endif">
                       <label for="department_from-field">department_from</label>
                       {!! Form::select('department_from', $department,old("department_from"),['class' => 'form-control','id'=>'department_from-field']); !!}
                       @if($errors->has("department_from"))
                        <span class="help-block">{{ $errors->first("department_from") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('user_from')) has-error @endif">
                       <label for="user_from-field">user_from</label>
                       {!! Form::select('user_from', $member,old("user_from"),['class' => 'form-control','id'=>'user_from-field']); !!}
                       @if($errors->has("user_from"))
                        <span class="help-block">{{ $errors->first("user_from") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('department_from')) has-error @endif">
                       <label for="department_from-field">department_from</label>
                       {!! Form::select('department_from', $department,old("department_from"),['class' => 'form-control','id'=>'department_from-field']); !!}
                       @if($errors->has("department_from"))
                        <span class="help-block">{{ $errors->first("department_from") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('user_to')) has-error @endif">
                       <label for="user_to-field">user_to</label>
                       {!! Form::select('user_to', $member,old("user_to"),['class' => 'form-control','id'=>'user_to-field']); !!}
                       @if($errors->has("user_to"))
                        <span class="help-block">{{ $errors->first("user_to") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('department_to')) has-error @endif">
                       <label for="department_to-field">department_to</label>
                       {!! Form::select('department_to', $department,old("department_to"),['class' => 'form-control','id'=>'department_to-field']); !!}
                       @if($errors->has("department_to"))
                        <span class="help-block">{{ $errors->first("department_to") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('inventories')) has-error @endif">
                       <label for="inventories-field">Inventories</label>
                       {!! Form::select('inventories[]', $inventory,old("inventories"),['class' => 'form-control','class'=>'js-example-basic-multiple','multiple'=>'multiple' ,'id'=>'inventories-field']); !!}
                       @if($errors->has("inventories"))
                        <span class="help-block">{{ $errors->first("inventories") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('mutations.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  x  <script>
    $('.date-picker').datepicker({
      format: 'yyyy-mm-dd',
      todayHighlight: true
    });
  </script>
  <script type="text/javascript">
$(".js-example-basic-multiple").select2(
  {
    placeholder: "Select a Inventories, don't duplicate",
    dropdownAutoWidth : true,
    width: '100%',
    tags: true,
  tokenSeparators: [',', ' ']
  });
</script>
@endsection
