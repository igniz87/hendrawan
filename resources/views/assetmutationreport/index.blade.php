@extends('layout')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">

@endsection
@section('header')
    <div class="page-header clearfix">
        <h1>PT. Fajar Sunmaster</h1>
        <h4>Report Date: <?php echo date("m/d/Y h:i:s a", time()); ?></h4>
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Assets Mutation Reports
        </h1>
    </div>
@endsection

@section('content')
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>PMA Number</th>
            <th>Asset Number</th>
            <th>Desc</th>
            <th>Dept From</th>
            <th>Dept To</th>
            <th>Mutation Date</th>
            <th>Reason For Mutation</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>PMA Number</th>
            <th>Asset Number</th>
            <th>Desc</th>
            <th>Dept From</th>
            <th>Dept To</th>
            <th>Mutation Date</th>
            <th>Reason For Mutation</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach ($mutations as $mutation) :?>
        <tr>
            <td><?php echo $mutation->id;?></td>
            <td><?php echo $mutation->nomor_pma;?></td>
            <td><?php echo $mutation->detail->inv->assets_number;?></td>
            <td><?php echo $mutation->keterangan;?></td>
            <td><?php echo $mutation->detail->deptFrom->name;?></td>
            <td><?php echo $mutation->detail->deptTo->name;?></td>
            <td><?php echo $mutation->tanggal_mutasi;?></td>
            <td><?php echo $mutation->keterangan;?></td>

        </tr>
        <?php endforeach;?>
        </tbody>
    </table>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#example').DataTable( {
          dom: 'Bfrtip',
          buttons: [
            'print'
          ]
        } );
      } );

    </script>
@endsection