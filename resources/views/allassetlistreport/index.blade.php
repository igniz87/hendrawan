@extends('layout')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">

@endsection
@section('header')
    <div class="page-header clearfix">
        <h1>PT. Fajar Sunmaster</h1>
        <h4>Report Date: <?php echo date("m/d/Y h:i:s a", time()); ?></h4>
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> All Asset List Reports
        </h1>
    </div>
@endsection

@section('content')
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Desc</th>
            <th>Acq Value (IDR)</th>
            <th>Acq Date</th>
            <th>Supplier</th>
            <th>Procurement</th>
            <th>Category</th>
            <th>Department</th>
            <th>User</th>
            <th>Useful Life</th>
            <th>Dept Per Month</th>
            <th>Book Value</th>
            <th>Desc 2</th>
            <th>Asset Number</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Desc</th>
            <th>Acq Value (IDR)</th>
            <th>Acq Date</th>
            <th>Supplier</th>
            <th>Procurement</th>
            <th>Category</th>
            <th>Department</th>
            <th>User</th>
            <th>Useful Life</th>
            <th>Dept Per Month</th>
            <th>Book Value</th>
            <th>Desc 2</th>
            <th>Asset Number</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach ($inventories as $inventory) :?>
        <tr>
            <td><?php echo $inventory->id;?></td>
            <td><?php echo $inventory->name;?></td>
            <td><?php echo $inventory->description;?></td>
            <td><?php echo $inventory->harga_beli;?></td>
            <td><?php echo $inventory->tanggal_beli;?></td>
            <td><?php echo $inventory->supp->name;?></td>
            <td><?php echo $inventory->proc->name;?></td>
            <td><?php echo $inventory->cat->name;?></td>
            <td><?php echo $inventory->dept->name;?></td>
            <td><?php echo $inventory->member->name;?></td>
            <td><?php echo $inventory->life;?></td>
            <?php
            $permonth = floor($inventory->harga_beli/($inventory->life*12));
            $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $inventory->tanggal_beli);
            $endDate = \Carbon\Carbon::now();
            $datediff = $startDate->diffInDays($endDate);
            $monthdiff = floor($datediff/30); ?>
            <td><?php echo floor($inventory->harga_beli/($inventory->life*12));?></td>
            <td><?php echo $inventory->harga_beli - ($monthdiff*$permonth);?></td>
            <td><?php echo $inventory->keterangan;?></td>
            <td><?php echo $inventory->assets_number;?></td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        'print'
      ]
    } );
  } );

</script>
@endsection