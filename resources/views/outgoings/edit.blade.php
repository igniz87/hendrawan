@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Outgoings / Edit #{{$outgoing->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('outgoings.update', $outgoing->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('inventory_id')) has-error @endif">
                       <label for="inventory_id-field">Inventory_id</label>
                    <input type="text" id="inventory_id-field" name="inventory_id" class="form-control" value="{{ is_null(old("inventory_id")) ? $outgoing->inventory_id : old("inventory_id") }}"/>
                       @if($errors->has("inventory_id"))
                        <span class="help-block">{{ $errors->first("inventory_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('keterangan')) has-error @endif">
                       <label for="keterangan-field">Notes</label>
                    <input type="text" id="keterangan-field" name="keterangan" class="form-control" value="{{ is_null(old("keterangan")) ? $outgoing->keterangan : old("keterangan") }}"/>
                       @if($errors->has("keterangan"))
                        <span class="help-block">{{ $errors->first("keterangan") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('outgoings.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
