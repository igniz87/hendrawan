@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Outgoings
            <a class="btn btn-success pull-right" href="{{ route('outgoings.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($outgoings->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>INVENTORY_NAME</th>
                            <th>PURCHASE_DATE</th>
                            <th>NOTES</th>
                           {{-- <th class="text-right">OPTIONS</th>--}}
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($outgoings as $outgoing)
                            <tr>
                                <td>{{$outgoing->id}}</td>
                                <td>ID: {{$outgoing->inv->id}} - Name: {{$outgoing->inv->name}} - Assets Number: {{$outgoing->inv->assets_number}}</td>
                                <td>Date: {{$outgoing->inv->tanggal_beli}}</td>
                    <td>{{$outgoing->keterangan}}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $outgoings->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection