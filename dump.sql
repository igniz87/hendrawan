/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost:3306
 Source Schema         : hendrawan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : 65001

 Date: 12/08/2017 09:37:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
BEGIN;
INSERT INTO `categories` VALUES (1, 'Computer', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `categories` VALUES (2, 'Desk', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `categories` VALUES (3, 'Planner', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `categories` VALUES (4, 'Automobiles', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `categories` VALUES (5, 'Tools', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of departments
-- ----------------------------
BEGIN;
INSERT INTO `departments` VALUES (1, 'Accounting', 'Ipsa sed qui fugit.', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `departments` VALUES (2, 'Business', 'Veniam ipsam sapiente consequatur nemo vero.', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `departments` VALUES (3, 'IT', 'Earum placeat voluptatem ipsam accusamus.', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `departments` VALUES (4, 'Human Resources', 'Perspiciatis sunt in magni ut enim sed rerum.', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `departments` VALUES (5, 'General Affairs', 'Fugiat rerum nihil non praesentium.', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

-- ----------------------------
-- Table structure for inventories
-- ----------------------------
DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga_beli` double NOT NULL,
  `tanggal_beli` date NOT NULL,
  `supplier_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobility_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `procurement_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `assets_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `life` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inventories
-- ----------------------------
BEGIN;
INSERT INTO `inventories` VALUES (1, 'Ignasius Wahyudi', '131231', 500000, '2016-10-12', '1', 'low', '1', '1', '1', '4', 'tes', 'C000001', 0, 2, '2017-05-14 14:39:01', '2017-07-24 16:30:00');
INSERT INTO `inventories` VALUES (2, 'Ignasius Wahyudi', '131231', 500000, '2016-10-12', '1', 'medium', '1', '2', '1', '4', 'tes', 'C000002', 0, 2, '2017-05-14 14:39:01', '2017-07-24 16:30:00');
INSERT INTO `inventories` VALUES (3, 'Ignasius Wahyudi', '131231', 500000, '2016-10-12', '1', 'high', '1', '3', '1', '4', 'tes', 'C000003', 0, 2, '2017-05-14 14:39:01', '2017-07-24 16:30:00');
INSERT INTO `inventories` VALUES (4, 'Ignasius Wahyudi', '131231', 500000, '2016-10-12', '1', 'medium', '1', '3', '1', '4', 'tes', 'C000004', 0, 2, '2017-05-14 14:39:01', '2017-07-24 16:30:00');
COMMIT;

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
BEGIN;
INSERT INTO `members` VALUES (1, 'Vivian Cassin', '4', '9787686397273', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (2, 'Julian Kshlerin', '2', '9788477487890', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (3, 'Krystina Lueilwitz', '5', '9789224080395', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (4, 'Magnus Connelly', '5', '9790201356860', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (5, 'Dr. Dangelo Muller', '5', '9794581726398', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (6, 'Halle Weissnat PhD', '5', '9787596303531', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (7, 'Zella Powlowski', '1', '9794114383050', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (8, 'Margaretta Nolan', '1', '9795783272355', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (9, 'Mrs. Shanna Kerluke', '3', '9789974462595', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `members` VALUES (10, 'Columbus Turner', '2', '9798761521490', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (79, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (80, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (81, '2016_10_08_150926_create_departments_table', 1);
INSERT INTO `migrations` VALUES (82, '2016_10_08_151039_create_suppliers_table', 1);
INSERT INTO `migrations` VALUES (83, '2016_10_08_151326_create_members_table', 1);
INSERT INTO `migrations` VALUES (84, '2016_10_08_151425_create_categories_table', 1);
INSERT INTO `migrations` VALUES (85, '2016_10_08_152220_create_procurements_table', 1);
INSERT INTO `migrations` VALUES (86, '2016_10_08_152540_create_inventories_table', 1);
INSERT INTO `migrations` VALUES (87, '2016_10_08_152916_create_mutations_table', 1);
INSERT INTO `migrations` VALUES (88, '2017_01_22_102057_create_outgoings_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for mutations
-- ----------------------------
DROP TABLE IF EXISTS `mutations`;
CREATE TABLE `mutations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_mutasi` date NOT NULL,
  `nomor_pma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mutations
-- ----------------------------
BEGIN;
INSERT INTO `mutations` VALUES (1, 'tess', '2017-07-24', '13312312', '2017-07-24 16:09:50', '2017-07-24 16:09:50');
COMMIT;

-- ----------------------------
-- Table structure for mutations_detail
-- ----------------------------
DROP TABLE IF EXISTS `mutations_detail`;
CREATE TABLE `mutations_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mutation_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `department_from` int(11) NOT NULL,
  `department_to` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mutations_detail
-- ----------------------------
BEGIN;
INSERT INTO `mutations_detail` VALUES (1, 1, 1, 1, 1, 1, 4, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for outgoings
-- ----------------------------
DROP TABLE IF EXISTS `outgoings`;
CREATE TABLE `outgoings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of outgoings
-- ----------------------------
BEGIN;
INSERT INTO `outgoings` VALUES (1, 1, 'tes', '2017-07-24 16:30:00', '2017-07-24 16:30:00');
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for procurements
-- ----------------------------
DROP TABLE IF EXISTS `procurements`;
CREATE TABLE `procurements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of procurements
-- ----------------------------
BEGIN;
INSERT INTO `procurements` VALUES (1, 'Buy', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `procurements` VALUES (2, 'Rent', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `procurements` VALUES (3, 'Giveaway', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
BEGIN;
INSERT INTO `suppliers` VALUES (1, 'Lucile Brakus I', '854 Stroman Roads Apt. 108\nNorth Drake, OR 76961', '+3494496948108', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (2, 'Malinda Emard', '585 Annabel Pines Suite 731\nFlavieville, FL 01951', '+2617261427922', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (3, 'Narciso Stamm', '80535 Hansen Mission Apt. 244\nJohnathanshire, SC 55076-1454', '+1926427456540', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (4, 'Dr. Alden Considine DVM', '92290 Janelle Flats Suite 841\nWest Dulceburgh, ME 82010-8742', '+8413599285976', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (5, 'Nelda Goodwin', '8476 Pattie Burg Apt. 704\nStiedemanntown, TX 55048', '+5556049571487', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (6, 'Vernon Luettgen', '722 Elroy Cliff\nPort Mckenzie, WI 31103-3489', '+6108520785983', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (7, 'Nolan Denesik', '4785 Kale Common Apt. 640\nNorth Chelsey, LA 91603', '+8500213707897', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (8, 'Shirley Wisozk', '70527 Maggio Turnpike\nMcClurefort, KY 84601-8504', '+4401397962367', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (9, 'Miss Karine Gislason', '80071 Mariana Valleys Apt. 242\nNorth Nick, MO 44734-9117', '+1255434566466', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (10, 'Betty Lueilwitz', '3962 Lavon Spurs\nKatharinamouth, WV 93478-9761', '+6568966766617', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (11, 'Rashad Hoeger PhD', '5548 Balistreri Cliff\nAdamsmouth, NH 74915-4128', '+3973347660063', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (12, 'Mathilde Murphy', '9894 Altenwerth Fort Apt. 473\nKattiestad, OK 59610', '+0267399714521', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (13, 'Gerry Emard', '4781 Enid Locks Suite 524\nRodriguezville, PA 03628', '+6936203163285', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (14, 'Modesta Kovacek', '644 Hester Court Suite 608\nKshlerinland, NV 62445', '+3602322025988', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (15, 'Kirsten Raynor V', '5357 Lowe Rest Apt. 901\nThompsonchester, IN 79854-1015', '+0262651609414', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (16, 'Tyree Okuneva IV', '54156 Pattie Highway Apt. 518\nPort Magaliberg, MS 77561-4371', '+5009760327283', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (17, 'Amparo Metz', '6510 Bernier Circles\nOsinskifurt, CO 24756', '+0554392577987', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (18, 'Maudie Lehner', '57979 Edyth Trafficway Suite 256\nDesmondberg, OK 26697', '+8525654524233', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (19, 'Hobart Heller', '97330 Joyce Neck Suite 215\nAbbottmouth, DC 95998-8555', '+6040596681945', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `suppliers` VALUES (20, 'Eric Effertz', '9732 Burley Cliffs\nBrycenberg, SC 99012-8971', '+4815931833426', '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Super Admin', 'superadmin@hendrawan.dev', '$2y$10$o5yNbAcenSF5HEi28.vJn./sh/z7VjsH/962Uhm34i3mFT3lotEGu', NULL, '2017-05-14 14:24:16', '2017-05-14 14:24:16');
INSERT INTO `users` VALUES (2, 'Lina O\'Connell', 'xorn@nienow.net', '$2y$10$Vwee5rsVbgLlYMsglD0Yt.MQO7heUgXTTTK3RZVjDkC9ocvehPaTO', NULL, '2017-05-14 14:24:16', '2017-05-14 14:24:16');
INSERT INTO `users` VALUES (3, 'Martine Schmitt', 'gislason.emie@hotmail.com', '$2y$10$Xxl96mAv8cO1v6iCCAkhTeU9MA.Ouzh5PBZ7vVAxYZhkPxn6x9tQe', NULL, '2017-05-14 14:24:16', '2017-05-14 14:24:16');
INSERT INTO `users` VALUES (4, 'Ms. Gregoria Ledner', 'langworth.bennie@zieme.info', '$2y$10$Bf.b1ujjyNvlM6klk7gBn.MNRUqb16ZFVg3/qoBwBcQmORx9IgmvW', NULL, '2017-05-14 14:24:16', '2017-05-14 14:24:16');
INSERT INTO `users` VALUES (5, 'Kian Armstrong', 'tracey93@homenick.info', '$2y$10$UyVONwxqwMFQtLmp2HL3aOL9qePWqDQfUtFY0wmqD1vNKCYLs5HZy', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (6, 'Hallie Labadie', 'mariano.romaguera@gmail.com', '$2y$10$7tLXiNdqbvt3fXR6EzgN4ejsH.n6zQrWcPfPbxJctqW923VOMTSz2', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (7, 'Johnathan Murphy', 'grayson91@gmail.com', '$2y$10$gcgLfjxLY7ZLy2Hf586pwemRWgPEHpyfnYVbD2fxc8zYYAnrEENVS', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (8, 'Mr. Nasir Kessler', 'kovacek.marcelino@rempel.com', '$2y$10$tgzJE..E3YGFT9kpY19OAeYip4mZyEuZUgtxOapzbLDPFDFmNJ4Sa', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (9, 'Roger Zulauf', 'pollich.leann@hotmail.com', '$2y$10$Sd7NarTdoD5dtEtxctcVle4D/gE8iMVxfY14Fv8P2JP2C/iCQaVn.', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (10, 'Deshawn Blick', 'gebert@kertzmann.com', '$2y$10$oUHfG6OpxPDk44x3TsD6kOPUBmJpIfw8Vsh6uevMQfU20i0ecbdfi', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
INSERT INTO `users` VALUES (11, 'Dr. Ashley Boyle', 'burnice41@yahoo.com', '$2y$10$AQ9tlCxxE5hkdah3F91/1ukn.CnX0syATVA3DMMLcfJ2Y6KXo.KxK', NULL, '2017-05-14 14:24:17', '2017-05-14 14:24:17');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
