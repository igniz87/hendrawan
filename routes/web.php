<?php

use App\Category;
use App\Department;
use App\Inventory;
use App\Member;
use App\Procurement;
use App\Supplier;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource("departments","DepartmentController");
Route::resource("suppliers","SupplierController");
Route::resource("members","MemberController");
Route::resource("categories","CategoryController");
Route::resource("procurements","ProcurementController");
Route::resource("inventories","InventoryController");
Route::resource("mutations","MutationController");
Route::resource("reports","ReportController");
Route::get('allassetlistreport', 'AllAssetListReportController@index')->name('allassetlistreport');
Route::get('supplierlistreport', 'SupplierListReportController@index')->name('supplierlistreport');
Route::get('memberlistreport', 'MemberListReportController@index')->name('memberlistreport');
Route::get('assetmutationreport', 'AssetMutationReportController@index')->name('assetmutationreport');
Route::get('assetscrappingreport', 'AssetScrappingReportController@index')->name('assetscrappingreport');
Route::get('assetbalancereport', 'AssetBalanceReportController@index')->name('assetbalancereport');
Route::get('specialassetbasedontypeofmobilityreport', 'SpecialAssetBasedOnTypeOfMobilityReportController@index')->name('specialassetbasedontypeofmobilityreport');
Route::resource("outgoings","OutgoingController"); // Add this line in routes.php

/*
    ___       _____   _  __
   /   |     / /   | | |/ /
  / /| |__  / / /| | |   / 
 / ___ / /_/ / ___ |/   |  
/_/  |_\____/_/  |_/_/|_|  
                           
*/

Route::get('/getAll', function() {

	$inventories = Inventory::all();

	$return = array(
'records' => $inventories,
'query_record_count' => sizeof($inventories),
'total_record_count' => sizeof($inventories)
		);
	// dd($car[0]->store_id);
   return $inventories;
});

Route::get('/getDepartment', function() {

	$departments = Department::all();

	;
	// dd($car[0]->store_id);
   return $departments;
});


Route::get('/getSupplier', function() {

	$suppliers = Supplier::all();

	;
	// dd($car[0]->store_id);
   return $suppliers;
});

Route::get('/getMember', function() {

	$members = Member::all();

	;
	// dd($car[0]->store_id);
   return $members;
});

Route::get('/getCategory', function() {

	$categories = Category::all();

	;
	// dd($car[0]->store_id);
	// dd($categories);
   return $categories;
});

Route::get('/getMobility', function() {

	$mobilities = 
collect(['low'=>'Low','medium'=>'Medium','high'=>'High']);
	// Category::all();

	;
	// dd($mobilities);
	// dd($car[0]->store_id);
   return $mobilities;
});


Route::get('/getProcurement', function() {

	$procurements = Procurement::all();

	;
	// dd($car[0]->store_id);
   return $procurements;
});


